#!/bin/bash

#
# Package lvmprovision.project.lvmtester
#
# Script: config.sh
#
# Configuration file for the 'lvm-tester' project.
#
# Naming conventions:
# * *_rel* prefix: Relative path.
# * *_abs* prefix: Absolute path.

# Ensure script robustness
# ========================
set -o errexit	# Fail immediatelly if any command fails.
set -o pipefail	# Fail immediatelly if any pipe stage fails.
set -o nounset	# Fail immediatelly if there is a derreference attempt on an unset variable. 

#
# Section: Required configuration
#

# Variable: target_host
# Name or IP address of the host to provision.
export target_host="10.1.198.106"

# Variable: local_username
# Username to use to authenticate in the target system.
export local_username=root

# Variable: project_id
# Unique identifier of the project. It can be whatever string that is valid as a directory name. You can generate one with the "uuidgen" program.
export project_id=289f5865-31eb-4c68-94ce-e3479e7e642d

